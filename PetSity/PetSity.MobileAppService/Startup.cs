using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(PetSity.MobileAppService.Startup))]

namespace PetSity.MobileAppService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}